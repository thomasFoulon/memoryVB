﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormStats
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lst_Nom = New System.Windows.Forms.ListBox()
        Me.CB_TrierPar = New System.Windows.Forms.ComboBox()
        Me.lbl_TrierPar = New System.Windows.Forms.Label()
        Me.lbl_TitreStats = New System.Windows.Forms.Label()
        Me.CB_Joueurs = New System.Windows.Forms.ComboBox()
        Me.lbl_AffStats = New System.Windows.Forms.Label()
        Me.btn_Stats = New System.Windows.Forms.Button()
        Me.btn_Retour = New System.Windows.Forms.Button()
        Me.lst_nbPartiesJouées = New System.Windows.Forms.ListBox()
        Me.lst_TempsDeJeu = New System.Windows.Forms.ListBox()
        Me.lst_MeilleurTemps = New System.Windows.Forms.ListBox()
        Me.lbl_Nom = New System.Windows.Forms.Label()
        Me.lbl_NbParties = New System.Windows.Forms.Label()
        Me.lbl_TpsJeu = New System.Windows.Forms.Label()
        Me.lbl_MeilleurTemps = New System.Windows.Forms.Label()
        Me.pnl_lsts = New System.Windows.Forms.Panel()
        Me.pnl_lsts.SuspendLayout()
        Me.SuspendLayout()
        '
        'lst_Nom
        '
        Me.lst_Nom.FormattingEnabled = True
        Me.lst_Nom.Location = New System.Drawing.Point(29, 3)
        Me.lst_Nom.Name = "lst_Nom"
        Me.lst_Nom.Size = New System.Drawing.Size(116, 212)
        Me.lst_Nom.TabIndex = 0
        '
        'CB_TrierPar
        '
        Me.CB_TrierPar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.CB_TrierPar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CB_TrierPar.FormattingEnabled = True
        Me.CB_TrierPar.Location = New System.Drawing.Point(257, 74)
        Me.CB_TrierPar.Name = "CB_TrierPar"
        Me.CB_TrierPar.Size = New System.Drawing.Size(191, 21)
        Me.CB_TrierPar.TabIndex = 1
        '
        'lbl_TrierPar
        '
        Me.lbl_TrierPar.AutoSize = True
        Me.lbl_TrierPar.Location = New System.Drawing.Point(199, 77)
        Me.lbl_TrierPar.Name = "lbl_TrierPar"
        Me.lbl_TrierPar.Size = New System.Drawing.Size(52, 13)
        Me.lbl_TrierPar.TabIndex = 2
        Me.lbl_TrierPar.Text = "Trier par :"
        '
        'lbl_TitreStats
        '
        Me.lbl_TitreStats.AutoSize = True
        Me.lbl_TitreStats.Font = New System.Drawing.Font("Garamond", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TitreStats.Location = New System.Drawing.Point(241, 12)
        Me.lbl_TitreStats.Name = "lbl_TitreStats"
        Me.lbl_TitreStats.Size = New System.Drawing.Size(179, 42)
        Me.lbl_TitreStats.TabIndex = 3
        Me.lbl_TitreStats.Text = "Statistiques"
        '
        'CB_Joueurs
        '
        Me.CB_Joueurs.FormattingEnabled = True
        Me.CB_Joueurs.Location = New System.Drawing.Point(154, 371)
        Me.CB_Joueurs.Name = "CB_Joueurs"
        Me.CB_Joueurs.Size = New System.Drawing.Size(121, 21)
        Me.CB_Joueurs.TabIndex = 4
        '
        'lbl_AffStats
        '
        Me.lbl_AffStats.AutoSize = True
        Me.lbl_AffStats.Location = New System.Drawing.Point(29, 374)
        Me.lbl_AffStats.Name = "lbl_AffStats"
        Me.lbl_AffStats.Size = New System.Drawing.Size(119, 13)
        Me.lbl_AffStats.TabIndex = 5
        Me.lbl_AffStats.Text = "Afficher statistiques de :"
        '
        'btn_Stats
        '
        Me.btn_Stats.Location = New System.Drawing.Point(291, 369)
        Me.btn_Stats.Name = "btn_Stats"
        Me.btn_Stats.Size = New System.Drawing.Size(75, 23)
        Me.btn_Stats.TabIndex = 6
        Me.btn_Stats.Text = "Statistiques"
        Me.btn_Stats.UseVisualStyleBackColor = True
        '
        'btn_Retour
        '
        Me.btn_Retour.Location = New System.Drawing.Point(564, 12)
        Me.btn_Retour.Name = "btn_Retour"
        Me.btn_Retour.Size = New System.Drawing.Size(75, 23)
        Me.btn_Retour.TabIndex = 7
        Me.btn_Retour.Text = "Retour"
        Me.btn_Retour.UseVisualStyleBackColor = True
        '
        'lst_nbPartiesJouées
        '
        Me.lst_nbPartiesJouées.FormattingEnabled = True
        Me.lst_nbPartiesJouées.Location = New System.Drawing.Point(180, 3)
        Me.lst_nbPartiesJouées.Name = "lst_nbPartiesJouées"
        Me.lst_nbPartiesJouées.Size = New System.Drawing.Size(116, 212)
        Me.lst_nbPartiesJouées.TabIndex = 8
        '
        'lst_TempsDeJeu
        '
        Me.lst_TempsDeJeu.FormattingEnabled = True
        Me.lst_TempsDeJeu.Location = New System.Drawing.Point(330, 3)
        Me.lst_TempsDeJeu.Name = "lst_TempsDeJeu"
        Me.lst_TempsDeJeu.Size = New System.Drawing.Size(116, 212)
        Me.lst_TempsDeJeu.TabIndex = 9
        '
        'lst_MeilleurTemps
        '
        Me.lst_MeilleurTemps.FormattingEnabled = True
        Me.lst_MeilleurTemps.Location = New System.Drawing.Point(480, 3)
        Me.lst_MeilleurTemps.Name = "lst_MeilleurTemps"
        Me.lst_MeilleurTemps.Size = New System.Drawing.Size(116, 212)
        Me.lst_MeilleurTemps.TabIndex = 10
        '
        'lbl_Nom
        '
        Me.lbl_Nom.AutoSize = True
        Me.lbl_Nom.Location = New System.Drawing.Point(57, 114)
        Me.lbl_Nom.Name = "lbl_Nom"
        Me.lbl_Nom.Size = New System.Drawing.Size(82, 13)
        Me.lbl_Nom.TabIndex = 11
        Me.lbl_Nom.Text = "Nom du joueur :"
        '
        'lbl_NbParties
        '
        Me.lbl_NbParties.AutoSize = True
        Me.lbl_NbParties.Location = New System.Drawing.Point(199, 114)
        Me.lbl_NbParties.Name = "lbl_NbParties"
        Me.lbl_NbParties.Size = New System.Drawing.Size(95, 13)
        Me.lbl_NbParties.TabIndex = 12
        Me.lbl_NbParties.Text = "Nb parties jouées :"
        '
        'lbl_TpsJeu
        '
        Me.lbl_TpsJeu.AutoSize = True
        Me.lbl_TpsJeu.Location = New System.Drawing.Point(360, 114)
        Me.lbl_TpsJeu.Name = "lbl_TpsJeu"
        Me.lbl_TpsJeu.Size = New System.Drawing.Size(77, 13)
        Me.lbl_TpsJeu.TabIndex = 13
        Me.lbl_TpsJeu.Text = "Temps de jeu :"
        '
        'lbl_MeilleurTemps
        '
        Me.lbl_MeilleurTemps.AutoSize = True
        Me.lbl_MeilleurTemps.Location = New System.Drawing.Point(507, 114)
        Me.lbl_MeilleurTemps.Name = "lbl_MeilleurTemps"
        Me.lbl_MeilleurTemps.Size = New System.Drawing.Size(80, 13)
        Me.lbl_MeilleurTemps.TabIndex = 14
        Me.lbl_MeilleurTemps.Text = "Meilleur temps :"
        '
        'pnl_lsts
        '
        Me.pnl_lsts.Controls.Add(Me.lst_Nom)
        Me.pnl_lsts.Controls.Add(Me.lst_nbPartiesJouées)
        Me.pnl_lsts.Controls.Add(Me.lst_TempsDeJeu)
        Me.pnl_lsts.Controls.Add(Me.lst_MeilleurTemps)
        Me.pnl_lsts.Location = New System.Drawing.Point(12, 130)
        Me.pnl_lsts.Name = "pnl_lsts"
        Me.pnl_lsts.Size = New System.Drawing.Size(614, 224)
        Me.pnl_lsts.TabIndex = 15
        '
        'FormStats
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 421)
        Me.Controls.Add(Me.pnl_lsts)
        Me.Controls.Add(Me.lbl_MeilleurTemps)
        Me.Controls.Add(Me.lbl_TpsJeu)
        Me.Controls.Add(Me.lbl_NbParties)
        Me.Controls.Add(Me.lbl_Nom)
        Me.Controls.Add(Me.btn_Retour)
        Me.Controls.Add(Me.btn_Stats)
        Me.Controls.Add(Me.lbl_AffStats)
        Me.Controls.Add(Me.CB_Joueurs)
        Me.Controls.Add(Me.lbl_TitreStats)
        Me.Controls.Add(Me.lbl_TrierPar)
        Me.Controls.Add(Me.CB_TrierPar)
        Me.Name = "FormStats"
        Me.Text = "Statistiques"
        Me.pnl_lsts.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lst_Nom As ListBox
    Friend WithEvents CB_TrierPar As ComboBox
    Friend WithEvents lbl_TrierPar As Label
    Friend WithEvents lbl_TitreStats As Label
    Friend WithEvents CB_Joueurs As ComboBox
    Friend WithEvents lbl_AffStats As Label
    Friend WithEvents btn_Stats As Button
    Friend WithEvents btn_Retour As Button
    Friend WithEvents lst_nbPartiesJouées As ListBox
    Friend WithEvents lst_TempsDeJeu As ListBox
    Friend WithEvents lst_MeilleurTemps As ListBox
    Friend WithEvents lbl_Nom As Label
    Friend WithEvents lbl_NbParties As Label
    Friend WithEvents lbl_TpsJeu As Label
    Friend WithEvents lbl_MeilleurTemps As Label
    Friend WithEvents pnl_lsts As Panel
End Class
