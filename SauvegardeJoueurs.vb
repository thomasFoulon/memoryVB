﻿Module SauvegardeJoueurs

    Public Const LONGUEUR_MAX_NOM As Integer = 20
    Private Const SECONDES_PAR_MINUTES As Integer = 60
    Public Structure Joueur
        <VBFixedString(LONGUEUR_MAX_NOM)> Dim nom As String
        Dim nbPartiesJouées As Integer
        Dim meilleurTemps, tempsDeJeu As Single
    End Structure

    Private joueursEnregistrés As Joueur()
    Private nbJoueursEnregistrés As Integer

    Public Function getJoueur(ByVal i As Integer) As Joueur
        Return joueursEnregistrés(i)
    End Function

    Public Function toMinSec(ByVal tps As Single) As String
        If tps = -1 Then
            Return "-"
        Else
            Dim minutesM As Integer = tps \ SECONDES_PAR_MINUTES
            Dim secondesM As Integer = tps Mod SECONDES_PAR_MINUTES
            Return (minutesM.ToString() & ":"c &
			If(secondesM <= 9, "0", "") & secondesM.ToString())
        End If
    End Function

    Public Function getNbJoueursEnregistrés() As Integer
        Return nbJoueursEnregistrés
    End Function

    Public Function toString(ByVal i As Integer) As String
        Return joueursEnregistrés(i).nom & "  " &
		joueursEnregistrés(i).nbPartiesJouées & "  " &
		joueursEnregistrés(i).tempsDeJeu & "  " &
		joueursEnregistrés(i).meilleurTemps
    End Function

    Public Function créerJoueur(ByVal nomJ As String) As Joueur
        Dim j As Joueur
        With j
            .nom = nomJ
            .nbPartiesJouées = 0
            .meilleurTemps = -1
            .tempsDeJeu = 0
        End With
        Return j
    End Function

    Public Sub nouveauJoueur(ByRef j As Joueur)
        Dim taille As Integer
        If joueursEnregistrés Is Nothing Then
            taille = -1
        Else
            taille = UBound(joueursEnregistrés)
        End If
        If nbJoueursEnregistrés >= taille Then
            ReDim Preserve joueursEnregistrés(taille + 1)
        End If
        joueursEnregistrés(nbJoueursEnregistrés) = j
        nbJoueursEnregistrés += 1
    End Sub

    Public Sub modifierInfosJoueur(ByVal i As Integer, ByVal j As Joueur)
        With joueursEnregistrés(i)
            .nom = j.nom
            .nbPartiesJouées = j.nbPartiesJouées
            .meilleurTemps = j.meilleurTemps
            .tempsDeJeu = j.tempsDeJeu
        End With
    End Sub

    Public Function joueurPrésent(ByVal nomJ As String) As Boolean
        Dim présent As Boolean = False
        For i As Integer = 0 To nbJoueursEnregistrés - 1
            If joueursEnregistrés(i).nom.Equals(nomJ) Then présent = True
        Next
        Return présent
    End Function

    Private Sub init()
        nbJoueursEnregistrés = 0
        'ouverture du fichier de sauvegarde
        Dim numF As Integer = FreeFile()
        FileOpen(numF, "./saves/save.sav", OpenMode.Random,
        OpenAccess.Read, OpenShare.LockWrite)
        While Not EOF(numF)
            Dim joueur As Joueur
            FileGet(numF, joueur)
            joueur.nom = Trim(joueur.nom)
            nouveauJoueur(joueur)
        End While
        FileClose(numF)

    End Sub

    Private Sub endApp()
        ' écriture dans le fichier
        Dim numF As Integer = FreeFile()
        FileOpen(numF, "./saves/save.sav", OpenMode.Random,
        OpenAccess.Write, OpenShare.LockReadWrite)
        For i As Integer = 0 To nbJoueursEnregistrés - 1
            FilePut(numF, joueursEnregistrés(i), i + 1)
        Next i
        FileClose(numF)
    End Sub

    Sub main()
        FormOptions.Show()
        FormOptions.Hide()
        init()
        ' lancement du formulaire d'accueil
        ' pour les nouveaux styles
        Application.EnableVisualStyles()
        Application.Run(FormAccueil)
        endApp()
    End Sub

End Module
