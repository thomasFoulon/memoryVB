﻿Public Class FormAccueil

    Private Sub FormAccueil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' paramètrage du bouton Jouer
        btn_Jouer.Enabled = False
        ' paramètrage de la ComboBox
        ComboB_Nom.AllowDrop = True
        ComboB_Nom.DropDownStyle = ComboBoxStyle.DropDown
        ComboB_Nom.AutoCompleteMode = AutoCompleteMode.Append
        ComboB_Nom.Sorted = True
        ComboB_Nom.MaxLength = SauvegardeJoueurs.LONGUEUR_MAX_NOM
        ' ajout des joueurs enregistrés à la ComboBox
        resetComboBox()

        PB_Image.Image = Image.FromFile("./images/logo-looneyTunes.png")
    End Sub

    Public Function getNomEntré() As String
        Return ComboB_Nom.Text
    End Function

    Private Sub resetComboBox()
        ComboB_Nom.Items.Clear()
        For i As Integer = 0 To SauvegardeJoueurs.getNbJoueursEnregistrés() - 1
            ComboB_Nom.Items.Add(getJoueur(i).nom)
        Next i
    End Sub

    Private Sub normalisationNom(sender As Object, e As EventArgs) Handles _
	ComboB_Nom.LostFocus
        ' vérifications préliminaires
        If sender.Text = "" Then Exit Sub
        If sender.Text.Length = 1 Then
            sender.Text = UCase(sender.Text)
            Exit Sub
        End If

        ' établissement du nouveau nom
        sender.Text = Trim(sender.Text)
        Dim newN As String = ""
        Dim noms As String() = sender.Text.Split(" "c)
        For Each nom As String In noms
            If Trim(nom) = "" Then Continue For
            Dim n As String = ""
            If nom.Length = 1 Then
                n = UCase(nom)
            Else
                n &= UCase(nom.Substring(0, 1))
                n &= LCase(nom.Substring(1, nom.Length - 1))
            End If
            newN &= n & " "c
        Next
        newN = RTrim(newN)

        ' remplacement de l'ancien nom par le nouveau
        sender.Text = newN
    End Sub

    Private Sub activ_désactiv_btn_Jouer(sender As ComboBox, e As EventArgs) Handles _
	ComboB_Nom.TextChanged
        ' si rien n'est entré, le bouton jouer est désactivé, sinon il l'est
        If sender.Text.Length > 0 Then
            btn_Jouer.Enabled = True
        Else
            ' si rien n'est entré, reset de la ComboBox
            resetComboBox()
            btn_Jouer.Enabled = False
        End If
    End Sub

    Private Sub btn_Jouer_Click(sender As Object, e As EventArgs) Handles btn_Jouer.Click
        If Not SauvegardeJoueurs.joueurPrésent(ComboB_Nom.Text) Then
            Dim joueur As Joueur = SauvegardeJoueurs.créerJoueur(ComboB_Nom.Text)
            SauvegardeJoueurs.nouveauJoueur(joueur)
        End If
        Me.Hide()
        FormJeu.Show()
    End Sub

    Private Sub btn_Scores_Click(sender As Object, e As EventArgs) Handles btn_Scores.Click
        Me.Hide()
        FormStats.Show()
    End Sub

    Private Sub btn_Options_Click(sender As Object, e As EventArgs) Handles btn_Options.Click
        Me.Hide()
        FormOptions.Show()
    End Sub

    Private Sub btn_Quitter_Click(sender As Object, e As EventArgs) Handles btn_Quitter.Click
        Dim reponse As MsgBoxResult
        reponse = MsgBox("Voulez-vous quitter l'application ?",
		MsgBoxStyle.YesNo, "Confirmation")
        If reponse = MsgBoxResult.Yes Then Me.Close()
    End Sub

End Class
