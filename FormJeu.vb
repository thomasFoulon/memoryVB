﻿Imports System.ComponentModel

Public Class FormJeu

    Private cartesAJouer As String
    Private dosCarte As Image
    Private images As Image()
    Private nbImages As Integer()

    Private Const NB_IMAGES_MAX As Integer = 5
    Private Const NB_CARTES_PAR_IMAGE As Integer = 4

    Private nbCartesRetournées As Integer
    Private cartesRetournées As Label()

    Private nbQuadruplets As Integer

    Private TPS_DE_JEU As Integer
    Private Const SECONDES_PAR_MINUTE As Integer = 60
    Private tpsRestant As Single

    Private indiceJoueurEnCours As Integer
    Private joueur As SauvegardeJoueurs.Joueur

    Private enCours As Boolean = False

    Public Function getTempsDeJeu()
        Return TPS_DE_JEU
    End Function

    Public Sub setCartesAJouer(carte As String)
        cartesAJouer = carte
    End Sub

    Public Sub setTempsDeJeu(tps As Integer)
        TPS_DE_JEU = tps
    End Sub

    Private Sub imagesToSet()
        If cartesAJouer.Equals("LooneyTunes") Then
            dosCarte = Image.FromFile("./images/LT/Dos_LooneyTunes.png")
            images(0) = Image.FromFile("./images/LT/Carte_BugsBunny.png")
            images(1) = Image.FromFile("./images/LT/Carte_DaffyDuck.png")
            images(2) = Image.FromFile("./images/LT/Carte_PorkyPig.png")
            images(3) = Image.FromFile("./images/LT/Carte_Taz.png")
            images(4) = Image.FromFile("./images/LT/Carte_TitiGrosminet.png")
            Exit Sub
        End If
        If cartesAJouer.Equals("TheWitcher") Then
            dosCarte = Image.FromFile("./images/GWENT/Dos_Gwent.png")
            images(0) = Image.FromFile("./images/GWENT/Geralt.png")
            images(1) = Image.FromFile("./images/GWENT/Griffon.png")
            images(2) = Image.FromFile("./images/GWENT/Iris.png")
            images(3) = Image.FromFile("./images/GWENT/Wolf.png")
            images(4) = Image.FromFile("./images/GWENT/Yennefer.png")
            Exit Sub
        End If
    End Sub

    Private Sub setImages()
        ' détermination des images associées à chaque carte
        Dim rd As New Random()
        Dim toTake As Integer = -1
        For Each carte As Label In pnl_Cartes.Controls
            ReDim carte.Tag(2)
            carte.Image = dosCarte
            carte.Tag(0) = carte.Image
            Do
                toTake = rd.Next(0, NB_IMAGES_MAX)
            Loop Until nbImages(toTake) < NB_CARTES_PAR_IMAGE
            carte.Tag(1) = images(toTake)
            carte.Tag(2) = toTake
            nbImages(toTake) += 1
        Next
    End Sub

    Private Sub FormJeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' dimensionnement des deux tableaux
        ReDim images(NB_IMAGES_MAX - 1)
        ReDim nbImages(NB_IMAGES_MAX - 1)

        ' initialisation du nombre d'images déjà associées
        For i As Integer = 0 To NB_IMAGES_MAX - 1
            nbImages(i) = 0
        Next i

        ' récupération options
        cartesAJouer = FormOptions.getCartesAJouer()
        TPS_DE_JEU = FormOptions.getTempsDeJeu()

        ' initialisation des images
        imagesToSet()
        setImages()

        'initialisation des autres informations
        nbCartesRetournées = 0
        ReDim cartesRetournées(NB_CARTES_PAR_IMAGE - 1)
        nbQuadruplets = 0

        ' TIMER
        ' toutes les secondes
        monTimer.Interval = 1000
        ' timer pas encore activé
        monTimer.Enabled = False
        tpsRestant = TPS_DE_JEU
        Dim minutes As Integer = tpsRestant \ SECONDES_PAR_MINUTE
        Dim secondes As Integer = tpsRestant Mod SECONDES_PAR_MINUTE
        info_Tps.Text = minutes.ToString() & ":"c &
			If(secondes <= 9, "0", "") & secondes.ToString()

        'joueur
        For i As Integer = 0 To SauvegardeJoueurs.getNbJoueursEnregistrés - 1
            Dim jou As SauvegardeJoueurs.Joueur = SauvegardeJoueurs.getJoueur(i)
            If jou.nom.Equals(FormAccueil.getNomEntré()) Then
                indiceJoueurEnCours = i
            End If
        Next
        info_Joueur.Text = SauvegardeJoueurs.getJoueur(indiceJoueurEnCours).nom
        ' pour enregistrer les nouvelles statistiques du joueur
        joueur = SauvegardeJoueurs.getJoueur(indiceJoueurEnCours)

        info_Tps.ForeColor = Color.Black
    End Sub

    ' procédure trouvée sur internet permettant d'attentre un certain temps sans empêcher les évènements
    ' http://codes-sources.commentcamarche.net/source/5635-attendre-un-certains-temps-pause-sans-perdre-la-main-en-vb-net
    Private Sub hbwait(ByVal ms_to_wait As Long)
        Dim endwait As Double
        endwait = Environment.TickCount + ms_to_wait
        While Environment.TickCount < endwait
            System.Threading.Thread.Sleep(1)
            Application.DoEvents()
        End While
    End Sub

    Private Sub actualisationTpsRestant(sender As Object, e As EventArgs) Handles _
	monTimer.Tick
        tpsRestant -= 1
        info_Tps.Text = SauvegardeJoueurs.toMinSec(tpsRestant)
        If tpsRestant <= 15 Then
            info_Tps.ForeColor = Color.Red
        End If
        If tpsRestant <= 0 Then
            info_Tps.Text = "0:00"
            tpsRestant = 0
            stopperTimer()
            MsgBox("Temps écoulé ! Vous avez perdu ...", MsgBoxStyle.OkOnly +
                   MsgBoxStyle.Exclamation, "Temps écoulé")
            quitterFormJeu()
        End If
    End Sub

    Private Sub resetCartes()
        For i As Integer = 0 To nbCartesRetournées - 1
            cartesRetournées(i).Image = cartesRetournées(i).Tag(0)
        Next i
        nbCartesRetournées = 0
    End Sub

    Private Sub stopperTimer()
        monTimer.Stop()
        monTimer.Enabled = False
    End Sub

    Private Sub quitterFormJeu()
        If tpsRestant <> TPS_DE_JEU Then
            joueur.nbPartiesJouées += 1
        End If
        joueur.tempsDeJeu += Math.Round(TPS_DE_JEU - tpsRestant, 1)

        SauvegardeJoueurs.modifierInfosJoueur(indiceJoueurEnCours, joueur)
        ' l'évènement closing du formulaire affiche automatiquement le formulaire d'accueil
        Me.Close()
    End Sub

    Private Sub calculMeilleurScore()
        If joueur.meilleurTemps = -1 Then joueur.meilleurTemps = TPS_DE_JEU
        If Math.Round(TPS_DE_JEU - tpsRestant) < joueur.meilleurTemps Then
            joueur.meilleurTemps = Math.Round(TPS_DE_JEU - tpsRestant, 1)
            MsgBox("Vous avez battu votre record personnel ! Nouveau PB : " &
                   SauvegardeJoueurs.toMinSec(joueur.meilleurTemps), MsgBoxStyle.OkOnly,
                       "Félicitations")
        End If
    End Sub

    Private Sub lbl_Carte_Click(sender As Label, e As EventArgs) _
        Handles lbl_Carte1.Click, lbl_Carte2.Click,
        lbl_Carte3.Click, lbl_Carte4.Click, lbl_Carte5.Click,
        lbl_Carte6.Click, lbl_Carte7.Click, lbl_Carte8.Click,
        lbl_Carte9.Click, lbl_Carte10.Click, lbl_Carte11.Click,
        lbl_Carte12.Click, lbl_Carte13.Click, lbl_Carte14.Click,
        lbl_Carte15.Click, lbl_Carte16.Click, lbl_Carte17.Click,
        lbl_Carte18.Click, lbl_Carte19.Click, lbl_Carte20.Click

        If enCours Then Exit Sub

        ' la procédure est en train de s'exécuter
        enCours = True

        ' démarrage du Timer si pas activé
        If Not monTimer.Enabled Then monTimer.Start()

        ' pour éviter la triche --> cliquer plusieurs fois rapidement sur la même carte pour la valider
        For i As Integer = 0 To nbCartesRetournées - 1
            If cartesRetournées(i).Equals(sender) Then
                enCours = False
                Exit Sub
            End If
        Next i

        nbCartesRetournées += 1

        ' sécurité pour éviter les bugs
        If nbCartesRetournées <= NB_CARTES_PAR_IMAGE Then
            sender.Image = sender.Tag(1)
            cartesRetournées(nbCartesRetournées - 1) = sender
        End If

        ' si au moins deux cartes sont retournées
        If nbCartesRetournées >= 2 Then
            ' si la dernière retournée est différente des autres
            ' (et pas seulement de la première pour éviter les bugs et donc la triche)
            For i As Integer = 0 To nbCartesRetournées - 2
                If sender.Tag(2) <> cartesRetournées(i).Tag(2) Then
                    ' attente ... le temps que le joueur puisse voir la dernière carte qu'il a retournée
                    hbwait(500)
                    ' reset
                    resetCartes()
                    ' sortie de la procédure
                    enCours = False
                    Exit Sub
                End If
            Next i
        End If

        ' si le joueur a retourné toutes les cartes
        If nbCartesRetournées = 4 Then
            For Each carte As Label In cartesRetournées
                carte.Enabled = False
            Next
            nbQuadruplets += 1
            nbCartesRetournées = 0
        End If

        If nbQuadruplets = NB_IMAGES_MAX Then
            stopperTimer()
            MsgBox("Félicitations ! Vous avez gagné !",
				MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Bravo !")
            calculMeilleurScore()
            quitterFormJeu()
        End If
        enCours = False
    End Sub

    Private Sub btn_Abandon_Click(sender As Object, e As EventArgs) Handles btn_Abandon.Click
        monTimer.Stop()
        Dim reponse As MsgBoxResult
        reponse = MsgBox("Etes-vous sûr de vouloir abandonner la partie en cours ?",
			MsgBoxStyle.YesNo, "Confirmation")
        If reponse = MsgBoxResult.Yes Then
            quitterFormJeu()
        Else
            monTimer.Start()
        End If
    End Sub

    Private Sub FormJeu_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        FormAccueil.Show()
    End Sub
End Class