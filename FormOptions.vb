﻿Imports System.ComponentModel

Public Class FormOptions

    Private Const SECONDES_PAR_MINUTES As Integer = 60

    Private TPS_DE_JEU As Integer = 60

    Private cartesAJouer As String

    Public Function getTempsDeJeu() As Integer
        Return TPS_DE_JEU
    End Function

    Public Function getCartesAJouer() As String
        If RB_TheWitcher.Checked Then
            Return "TheWitcher"
        Else
            Return "LooneyTunes"
        End If
    End Function

    Private Sub FormOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        HSB_Tps.Minimum = 30
        HSB_Tps.LargeChange = 30
        HSB_Tps.SmallChange = 10
        HSB_Tps.Maximum = 180 + HSB_Tps.LargeChange - 1

        HSB_Tps.Value = SECONDES_PAR_MINUTES
        Dim minutes As Integer = HSB_Tps.Value \ SECONDES_PAR_MINUTES
        Dim secondes As Integer = HSB_Tps.Value Mod SECONDES_PAR_MINUTES
        info_Tps.Text = minutes & ":"c & If(secondes < 10, "0"c, "") & secondes

        Dos_1.Image = Image.FromFile("./images/LT/Dos_LooneyTunes.png")
        Carte_1.Image = Image.FromFile("./images/LT/Carte_BugsBunny.png")

        Dos_2.Image = Image.FromFile("./images/GWENT/Dos_Gwent.png")
        Carte_2.Image = Image.FromFile("./images/GWENT/Griffon.png")
    End Sub

    Private Sub HSB_Tps_Scroll(sender As Object, e As ScrollEventArgs) Handles HSB_Tps.Scroll
        HSB_Tps.Value = HSB_Tps.Value - HSB_Tps.Value Mod 10
        TPS_DE_JEU = HSB_Tps.Value
        Dim minutes As Integer = HSB_Tps.Value \ SECONDES_PAR_MINUTES
        Dim secondes As Integer = HSB_Tps.Value Mod SECONDES_PAR_MINUTES
        info_Tps.Text = minutes & ":"c & If(secondes < 10, "0"c, "") & secondes
    End Sub

    Private Sub btn_Confirmer_Click(sender As Object, e As EventArgs) Handles _
	btn_Confirmer.Click
        If RB_TheWitcher.Checked Then
            FormAccueil.PB_Image.Image = Image.FromFile("./images/Gwent_logo.png")
        End If
        If RB_LooneyTunes.Checked Then
            FormAccueil.PB_Image.Image = Image.FromFile("./images/logo-looneyTunes.png")
        End If
        Me.Hide()
        FormAccueil.Show()
    End Sub

    Private Sub FormOptions_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        FormAccueil.PB_Image.Image = Image.FromFile("./images/logo-looneyTunes.png")
        FormAccueil.Show()
    End Sub

End Class