﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormOptions
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_Titre = New System.Windows.Forms.Label()
        Me.HSB_Tps = New System.Windows.Forms.HScrollBar()
        Me.lbl_Tps = New System.Windows.Forms.Label()
        Me.info_Tps = New System.Windows.Forms.Label()
        Me.lbl_SelectTemps = New System.Windows.Forms.Label()
        Me.lbl_SelectStyle = New System.Windows.Forms.Label()
        Me.RB_LooneyTunes = New System.Windows.Forms.RadioButton()
        Me.RB_TheWitcher = New System.Windows.Forms.RadioButton()
        Me.Carte_1 = New System.Windows.Forms.PictureBox()
        Me.Dos_1 = New System.Windows.Forms.PictureBox()
        Me.Dos_2 = New System.Windows.Forms.PictureBox()
        Me.Carte_2 = New System.Windows.Forms.PictureBox()
        Me.btn_Confirmer = New System.Windows.Forms.Button()
        CType(Me.Carte_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dos_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dos_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Carte_2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_Titre
        '
        Me.lbl_Titre.AutoSize = True
        Me.lbl_Titre.Font = New System.Drawing.Font("Garamond", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Titre.Location = New System.Drawing.Point(203, 25)
        Me.lbl_Titre.Name = "lbl_Titre"
        Me.lbl_Titre.Size = New System.Drawing.Size(137, 42)
        Me.lbl_Titre.TabIndex = 0
        Me.lbl_Titre.Text = "Options"
        '
        'HSB_Tps
        '
        Me.HSB_Tps.LargeChange = 30
        Me.HSB_Tps.Location = New System.Drawing.Point(75, 165)
        Me.HSB_Tps.Maximum = 210
        Me.HSB_Tps.Minimum = 30
        Me.HSB_Tps.Name = "HSB_Tps"
        Me.HSB_Tps.Size = New System.Drawing.Size(408, 17)
        Me.HSB_Tps.SmallChange = 10
        Me.HSB_Tps.TabIndex = 1
        Me.HSB_Tps.Value = 30
        '
        'lbl_Tps
        '
        Me.lbl_Tps.AutoSize = True
        Me.lbl_Tps.Location = New System.Drawing.Point(225, 135)
        Me.lbl_Tps.Name = "lbl_Tps"
        Me.lbl_Tps.Size = New System.Drawing.Size(77, 13)
        Me.lbl_Tps.TabIndex = 2
        Me.lbl_Tps.Text = "Temps de jeu :"
        '
        'info_Tps
        '
        Me.info_Tps.AutoSize = True
        Me.info_Tps.Location = New System.Drawing.Point(308, 135)
        Me.info_Tps.Name = "info_Tps"
        Me.info_Tps.Size = New System.Drawing.Size(10, 13)
        Me.info_Tps.TabIndex = 3
        Me.info_Tps.Text = "-"
        '
        'lbl_SelectTemps
        '
        Me.lbl_SelectTemps.AutoSize = True
        Me.lbl_SelectTemps.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_SelectTemps.Location = New System.Drawing.Point(30, 93)
        Me.lbl_SelectTemps.Name = "lbl_SelectTemps"
        Me.lbl_SelectTemps.Size = New System.Drawing.Size(235, 20)
        Me.lbl_SelectTemps.TabIndex = 4
        Me.lbl_SelectTemps.Text = "- Sélectionnez un temps de jeu :"
        '
        'lbl_SelectStyle
        '
        Me.lbl_SelectStyle.AutoSize = True
        Me.lbl_SelectStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_SelectStyle.Location = New System.Drawing.Point(30, 234)
        Me.lbl_SelectStyle.Name = "lbl_SelectStyle"
        Me.lbl_SelectStyle.Size = New System.Drawing.Size(238, 20)
        Me.lbl_SelectStyle.TabIndex = 5
        Me.lbl_SelectStyle.Text = "- Sélectionnez un style de carte :"
        '
        'RB_LooneyTunes
        '
        Me.RB_LooneyTunes.AutoSize = True
        Me.RB_LooneyTunes.Location = New System.Drawing.Point(290, 291)
        Me.RB_LooneyTunes.Name = "RB_LooneyTunes"
        Me.RB_LooneyTunes.Size = New System.Drawing.Size(93, 17)
        Me.RB_LooneyTunes.TabIndex = 6
        Me.RB_LooneyTunes.TabStop = True
        Me.RB_LooneyTunes.Text = "Looney Tunes"
        Me.RB_LooneyTunes.UseVisualStyleBackColor = True
        '
        'RB_TheWitcher
        '
        Me.RB_TheWitcher.AutoSize = True
        Me.RB_TheWitcher.Location = New System.Drawing.Point(290, 352)
        Me.RB_TheWitcher.Name = "RB_TheWitcher"
        Me.RB_TheWitcher.Size = New System.Drawing.Size(124, 17)
        Me.RB_TheWitcher.TabIndex = 7
        Me.RB_TheWitcher.TabStop = True
        Me.RB_TheWitcher.Text = "The Witcher (Gwent)"
        Me.RB_TheWitcher.UseVisualStyleBackColor = True
        '
        'Carte_1
        '
        Me.Carte_1.Location = New System.Drawing.Point(241, 282)
        Me.Carte_1.Name = "Carte_1"
        Me.Carte_1.Size = New System.Drawing.Size(27, 38)
        Me.Carte_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Carte_1.TabIndex = 12
        Me.Carte_1.TabStop = False
        '
        'Dos_1
        '
        Me.Dos_1.Location = New System.Drawing.Point(194, 282)
        Me.Dos_1.Name = "Dos_1"
        Me.Dos_1.Size = New System.Drawing.Size(27, 38)
        Me.Dos_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Dos_1.TabIndex = 13
        Me.Dos_1.TabStop = False
        '
        'Dos_2
        '
        Me.Dos_2.Location = New System.Drawing.Point(194, 343)
        Me.Dos_2.Name = "Dos_2"
        Me.Dos_2.Size = New System.Drawing.Size(27, 38)
        Me.Dos_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Dos_2.TabIndex = 14
        Me.Dos_2.TabStop = False
        '
        'Carte_2
        '
        Me.Carte_2.Location = New System.Drawing.Point(241, 343)
        Me.Carte_2.Name = "Carte_2"
        Me.Carte_2.Size = New System.Drawing.Size(27, 38)
        Me.Carte_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Carte_2.TabIndex = 15
        Me.Carte_2.TabStop = False
        '
        'btn_Confirmer
        '
        Me.btn_Confirmer.Location = New System.Drawing.Point(194, 414)
        Me.btn_Confirmer.Name = "btn_Confirmer"
        Me.btn_Confirmer.Size = New System.Drawing.Size(155, 23)
        Me.btn_Confirmer.TabIndex = 16
        Me.btn_Confirmer.Text = "Confirmer les paramètres"
        Me.btn_Confirmer.UseVisualStyleBackColor = True
        '
        'FormOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 465)
        Me.Controls.Add(Me.btn_Confirmer)
        Me.Controls.Add(Me.Carte_2)
        Me.Controls.Add(Me.Dos_2)
        Me.Controls.Add(Me.Dos_1)
        Me.Controls.Add(Me.Carte_1)
        Me.Controls.Add(Me.RB_TheWitcher)
        Me.Controls.Add(Me.RB_LooneyTunes)
        Me.Controls.Add(Me.lbl_SelectStyle)
        Me.Controls.Add(Me.lbl_SelectTemps)
        Me.Controls.Add(Me.info_Tps)
        Me.Controls.Add(Me.lbl_Tps)
        Me.Controls.Add(Me.HSB_Tps)
        Me.Controls.Add(Me.lbl_Titre)
        Me.Name = "FormOptions"
        Me.Text = "Options"
        CType(Me.Carte_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dos_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dos_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Carte_2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_Titre As Label
    Friend WithEvents HSB_Tps As HScrollBar
    Friend WithEvents lbl_Tps As Label
    Friend WithEvents info_Tps As Label
    Friend WithEvents lbl_SelectTemps As Label
    Friend WithEvents lbl_SelectStyle As Label
    Friend WithEvents RB_LooneyTunes As RadioButton
    Friend WithEvents RB_TheWitcher As RadioButton
    Friend WithEvents Carte_1 As PictureBox
    Friend WithEvents Dos_1 As PictureBox
    Friend WithEvents Dos_2 As PictureBox
    Friend WithEvents Carte_2 As PictureBox
    Friend WithEvents btn_Confirmer As Button
End Class
