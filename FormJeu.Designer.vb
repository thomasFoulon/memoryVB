﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbl_Joueur = New System.Windows.Forms.Label()
        Me.info_Joueur = New System.Windows.Forms.Label()
        Me.lbl_Tps = New System.Windows.Forms.Label()
        Me.info_Tps = New System.Windows.Forms.Label()
        Me.btn_Abandon = New System.Windows.Forms.Button()
        Me.pnl_Cartes = New System.Windows.Forms.Panel()
        Me.lbl_Carte1 = New System.Windows.Forms.Label()
        Me.lbl_Carte20 = New System.Windows.Forms.Label()
        Me.lbl_Carte2 = New System.Windows.Forms.Label()
        Me.lbl_Carte19 = New System.Windows.Forms.Label()
        Me.lbl_Carte3 = New System.Windows.Forms.Label()
        Me.lbl_Carte18 = New System.Windows.Forms.Label()
        Me.lbl_Carte4 = New System.Windows.Forms.Label()
        Me.lbl_Carte17 = New System.Windows.Forms.Label()
        Me.lbl_Carte5 = New System.Windows.Forms.Label()
        Me.lbl_Carte16 = New System.Windows.Forms.Label()
        Me.lbl_Carte6 = New System.Windows.Forms.Label()
        Me.lbl_Carte15 = New System.Windows.Forms.Label()
        Me.lbl_Carte7 = New System.Windows.Forms.Label()
        Me.lbl_Carte14 = New System.Windows.Forms.Label()
        Me.lbl_Carte8 = New System.Windows.Forms.Label()
        Me.lbl_Carte13 = New System.Windows.Forms.Label()
        Me.lbl_Carte9 = New System.Windows.Forms.Label()
        Me.lbl_Carte12 = New System.Windows.Forms.Label()
        Me.lbl_Carte10 = New System.Windows.Forms.Label()
        Me.lbl_Carte11 = New System.Windows.Forms.Label()
        Me.monTimer = New System.Windows.Forms.Timer(Me.components)
        Me.pnl_Cartes.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_Joueur
        '
        Me.lbl_Joueur.AutoSize = True
        Me.lbl_Joueur.Location = New System.Drawing.Point(33, 26)
        Me.lbl_Joueur.Name = "lbl_Joueur"
        Me.lbl_Joueur.Size = New System.Drawing.Size(45, 13)
        Me.lbl_Joueur.TabIndex = 0
        Me.lbl_Joueur.Text = "Joueur :"
        '
        'info_Joueur
        '
        Me.info_Joueur.AutoSize = True
        Me.info_Joueur.Location = New System.Drawing.Point(84, 26)
        Me.info_Joueur.Name = "info_Joueur"
        Me.info_Joueur.Size = New System.Drawing.Size(10, 13)
        Me.info_Joueur.TabIndex = 1
        Me.info_Joueur.Text = "-"
        '
        'lbl_Tps
        '
        Me.lbl_Tps.AutoSize = True
        Me.lbl_Tps.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Tps.Location = New System.Drawing.Point(173, 18)
        Me.lbl_Tps.Name = "lbl_Tps"
        Me.lbl_Tps.Size = New System.Drawing.Size(139, 24)
        Me.lbl_Tps.TabIndex = 2
        Me.lbl_Tps.Text = "Temps restant :"
        '
        'info_Tps
        '
        Me.info_Tps.AutoSize = True
        Me.info_Tps.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.info_Tps.Location = New System.Drawing.Point(318, 18)
        Me.info_Tps.Name = "info_Tps"
        Me.info_Tps.Size = New System.Drawing.Size(16, 24)
        Me.info_Tps.TabIndex = 3
        Me.info_Tps.Text = "-"
        '
        'btn_Abandon
        '
        Me.btn_Abandon.Location = New System.Drawing.Point(408, 21)
        Me.btn_Abandon.Name = "btn_Abandon"
        Me.btn_Abandon.Size = New System.Drawing.Size(121, 23)
        Me.btn_Abandon.TabIndex = 4
        Me.btn_Abandon.Text = "Abandon de la partie"
        Me.btn_Abandon.UseVisualStyleBackColor = True
        '
        'pnl_Cartes
        '
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte1)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte20)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte2)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte19)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte3)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte18)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte4)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte17)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte5)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte16)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte6)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte15)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte7)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte14)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte8)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte13)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte9)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte12)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte10)
        Me.pnl_Cartes.Controls.Add(Me.lbl_Carte11)
        Me.pnl_Cartes.Location = New System.Drawing.Point(6, 59)
        Me.pnl_Cartes.Name = "pnl_Cartes"
        Me.pnl_Cartes.Size = New System.Drawing.Size(535, 557)
        Me.pnl_Cartes.TabIndex = 30
        '
        'lbl_Carte1
        '
        Me.lbl_Carte1.Location = New System.Drawing.Point(12, 10)
        Me.lbl_Carte1.Name = "lbl_Carte1"
        Me.lbl_Carte1.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte1.TabIndex = 10
        '
        'lbl_Carte20
        '
        Me.lbl_Carte20.Location = New System.Drawing.Point(433, 422)
        Me.lbl_Carte20.Name = "lbl_Carte20"
        Me.lbl_Carte20.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte20.TabIndex = 29
        '
        'lbl_Carte2
        '
        Me.lbl_Carte2.Location = New System.Drawing.Point(118, 10)
        Me.lbl_Carte2.Name = "lbl_Carte2"
        Me.lbl_Carte2.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte2.TabIndex = 11
        '
        'lbl_Carte19
        '
        Me.lbl_Carte19.Location = New System.Drawing.Point(328, 422)
        Me.lbl_Carte19.Name = "lbl_Carte19"
        Me.lbl_Carte19.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte19.TabIndex = 28
        '
        'lbl_Carte3
        '
        Me.lbl_Carte3.Location = New System.Drawing.Point(223, 10)
        Me.lbl_Carte3.Name = "lbl_Carte3"
        Me.lbl_Carte3.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte3.TabIndex = 12
        '
        'lbl_Carte18
        '
        Me.lbl_Carte18.Location = New System.Drawing.Point(223, 422)
        Me.lbl_Carte18.Name = "lbl_Carte18"
        Me.lbl_Carte18.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte18.TabIndex = 27
        '
        'lbl_Carte4
        '
        Me.lbl_Carte4.Location = New System.Drawing.Point(328, 10)
        Me.lbl_Carte4.Name = "lbl_Carte4"
        Me.lbl_Carte4.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte4.TabIndex = 13
        '
        'lbl_Carte17
        '
        Me.lbl_Carte17.Location = New System.Drawing.Point(118, 422)
        Me.lbl_Carte17.Name = "lbl_Carte17"
        Me.lbl_Carte17.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte17.TabIndex = 26
        '
        'lbl_Carte5
        '
        Me.lbl_Carte5.Location = New System.Drawing.Point(433, 10)
        Me.lbl_Carte5.Name = "lbl_Carte5"
        Me.lbl_Carte5.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte5.TabIndex = 14
        '
        'lbl_Carte16
        '
        Me.lbl_Carte16.Location = New System.Drawing.Point(12, 422)
        Me.lbl_Carte16.Name = "lbl_Carte16"
        Me.lbl_Carte16.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte16.TabIndex = 25
        '
        'lbl_Carte6
        '
        Me.lbl_Carte6.Location = New System.Drawing.Point(12, 146)
        Me.lbl_Carte6.Name = "lbl_Carte6"
        Me.lbl_Carte6.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte6.TabIndex = 15
        '
        'lbl_Carte15
        '
        Me.lbl_Carte15.Location = New System.Drawing.Point(433, 283)
        Me.lbl_Carte15.Name = "lbl_Carte15"
        Me.lbl_Carte15.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte15.TabIndex = 24
        '
        'lbl_Carte7
        '
        Me.lbl_Carte7.Location = New System.Drawing.Point(118, 146)
        Me.lbl_Carte7.Name = "lbl_Carte7"
        Me.lbl_Carte7.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte7.TabIndex = 16
        '
        'lbl_Carte14
        '
        Me.lbl_Carte14.Location = New System.Drawing.Point(328, 283)
        Me.lbl_Carte14.Name = "lbl_Carte14"
        Me.lbl_Carte14.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte14.TabIndex = 23
        '
        'lbl_Carte8
        '
        Me.lbl_Carte8.Location = New System.Drawing.Point(223, 146)
        Me.lbl_Carte8.Name = "lbl_Carte8"
        Me.lbl_Carte8.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte8.TabIndex = 17
        '
        'lbl_Carte13
        '
        Me.lbl_Carte13.Location = New System.Drawing.Point(223, 283)
        Me.lbl_Carte13.Name = "lbl_Carte13"
        Me.lbl_Carte13.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte13.TabIndex = 22
        '
        'lbl_Carte9
        '
        Me.lbl_Carte9.Location = New System.Drawing.Point(328, 146)
        Me.lbl_Carte9.Name = "lbl_Carte9"
        Me.lbl_Carte9.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte9.TabIndex = 18
        '
        'lbl_Carte12
        '
        Me.lbl_Carte12.Location = New System.Drawing.Point(118, 283)
        Me.lbl_Carte12.Name = "lbl_Carte12"
        Me.lbl_Carte12.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte12.TabIndex = 21
        '
        'lbl_Carte10
        '
        Me.lbl_Carte10.Location = New System.Drawing.Point(433, 146)
        Me.lbl_Carte10.Name = "lbl_Carte10"
        Me.lbl_Carte10.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte10.TabIndex = 19
        '
        'lbl_Carte11
        '
        Me.lbl_Carte11.Location = New System.Drawing.Point(12, 283)
        Me.lbl_Carte11.Name = "lbl_Carte11"
        Me.lbl_Carte11.Size = New System.Drawing.Size(90, 125)
        Me.lbl_Carte11.TabIndex = 20
        '
        'monTimer
        '
        '
        'FormJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(553, 625)
        Me.Controls.Add(Me.pnl_Cartes)
        Me.Controls.Add(Me.btn_Abandon)
        Me.Controls.Add(Me.info_Tps)
        Me.Controls.Add(Me.lbl_Tps)
        Me.Controls.Add(Me.info_Joueur)
        Me.Controls.Add(Me.lbl_Joueur)
        Me.Name = "FormJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Memory"
        Me.pnl_Cartes.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_Joueur As Label
    Friend WithEvents info_Joueur As Label
    Friend WithEvents lbl_Tps As Label
    Friend WithEvents info_Tps As Label
    Friend WithEvents btn_Abandon As Button
    Friend WithEvents lbl_Carte5 As Label
    Friend WithEvents lbl_Carte4 As Label
    Friend WithEvents lbl_Carte3 As Label
    Friend WithEvents lbl_Carte2 As Label
    Friend WithEvents lbl_Carte1 As Label
    Friend WithEvents lbl_Carte10 As Label
    Friend WithEvents lbl_Carte9 As Label
    Friend WithEvents lbl_Carte8 As Label
    Friend WithEvents lbl_Carte7 As Label
    Friend WithEvents lbl_Carte6 As Label
    Friend WithEvents lbl_Carte15 As Label
    Friend WithEvents lbl_Carte14 As Label
    Friend WithEvents lbl_Carte13 As Label
    Friend WithEvents lbl_Carte12 As Label
    Friend WithEvents lbl_Carte11 As Label
    Friend WithEvents lbl_Carte20 As Label
    Friend WithEvents lbl_Carte19 As Label
    Friend WithEvents lbl_Carte18 As Label
    Friend WithEvents lbl_Carte17 As Label
    Friend WithEvents lbl_Carte16 As Label
    Friend WithEvents pnl_Cartes As Panel
    Friend WithEvents monTimer As Timer
End Class
