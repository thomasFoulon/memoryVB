﻿Imports System.ComponentModel

Public Class FormStats

    Private Const SECONDES_PAR_MINUTES = 60

    Private Sub FormStats_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        FormAccueil.Show()
    End Sub

    Private Sub changeList(ByVal joueurs As SauvegardeJoueurs.Joueur())
        lst_Nom.Items.Clear()
        lst_nbPartiesJouées.Items.Clear()
        lst_TempsDeJeu.Items.Clear()
        lst_MeilleurTemps.Items.Clear()

        For i As Integer = 0 To UBound(joueurs)
            Dim joueur As SauvegardeJoueurs.Joueur = joueurs(i)
            lst_Nom.Items.Add(joueur.nom)
            lst_nbPartiesJouées.Items.Add(joueur.nbPartiesJouées)
            lst_TempsDeJeu.Items.Add(toMinSec(joueur.tempsDeJeu))
            If joueur.meilleurTemps = -1 Then
                lst_MeilleurTemps.Items.Add("-")
            Else
                lst_MeilleurTemps.Items.Add(toMinSec(joueur.meilleurTemps))
            End If
        Next i
    End Sub

    Private Sub FormStats_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CB_TrierPar.Items.Add("Ordre alphabétique")
        CB_TrierPar.Items.Add("Meilleur temps")

        lst_Nom.Sorted = False
        lst_nbPartiesJouées.Sorted = False
        lst_TempsDeJeu.Sorted = False
        lst_MeilleurTemps.Sorted = False

        triAlphabétique()

        For i As Integer = 0 To SauvegardeJoueurs.getNbJoueursEnregistrés() - 1
            CB_Joueurs.Items.Add(SauvegardeJoueurs.getJoueur(i).nom)
        Next i
    End Sub

    Private Sub btn_Retour_Click(sender As Object, e As EventArgs) Handles btn_Retour.Click
        Me.Close()
    End Sub

    Private Sub lst_SelectedIndexChanged(sender As ListBox, e As EventArgs) _
        Handles lst_Nom.SelectedIndexChanged, lst_nbPartiesJouées.SelectedIndexChanged,
        lst_TempsDeJeu.SelectedIndexChanged, lst_MeilleurTemps.SelectedIndexChanged

        Dim ind As Integer = sender.SelectedIndex

        For Each l As ListBox In pnl_lsts.Controls
            l.SelectedIndex = ind
        Next
    End Sub

    Private Function getNewTabJoueurs() As SauvegardeJoueurs.Joueur()
        Dim newTab As SauvegardeJoueurs.Joueur()
        ReDim newTab(SauvegardeJoueurs.getNbJoueursEnregistrés() - 1)
        For i As Integer = 0 To SauvegardeJoueurs.getNbJoueursEnregistrés() - 1
            newTab(i) = SauvegardeJoueurs.getJoueur(i)
        Next i
        Return newTab
    End Function

    Private Sub triAlphabétique()
        Dim newTab As SauvegardeJoueurs.Joueur() = getNewTabJoueurs()
        For i As Integer = 0 To UBound(newTab) - 1
            Dim min As Integer = i
            For j As Integer = i + 1 To UBound(newTab)
                If newTab(j).nom < newTab(min).nom Then
                    min = j
                End If
            Next j
            If min <> i Then
                Dim joueur As SauvegardeJoueurs.Joueur
                joueur = newTab(i)
                newTab(i) = newTab(min)
                newTab(min) = joueur
            End If
        Next i
        changeList(newTab)
    End Sub

    Private Sub triMeilleurTemps()
        Dim newTab As SauvegardeJoueurs.Joueur() = getNewTabJoueurs()
        For i As Integer = 0 To UBound(newTab) - 1
            Dim min As Integer = i
            If newTab(min).meilleurTemps = -1 Then
                min = i + 1
            End If
            For j As Integer = i + 1 To UBound(newTab)
                If (newTab(j).meilleurTemps < newTab(min).meilleurTemps) AndAlso
                    newTab(j).meilleurTemps <> -1 Then
                    min = j
                End If
            Next j
            If min <> i Then
                Dim joueur As SauvegardeJoueurs.Joueur
                joueur = newTab(i)
                newTab(i) = newTab(min)
                newTab(min) = joueur
            End If
        Next i
        changeList(newTab)
    End Sub

    Private Sub CB_TrierPar_TextChanged(sender As ComboBox, e As EventArgs) Handles _
		CB_TrierPar.TextChanged
        If sender.Text.Equals("Ordre alphabétique") Then
            triAlphabétique()
        ElseIf sender.Text.Equals("Meilleur temps") Then
            triMeilleurTemps()
        End If
    End Sub

    Private Sub btn_Stats_Click(sender As Object, e As EventArgs) Handles btn_Stats.Click
        Dim j As Joueur = SauvegardeJoueurs.getJoueur(CB_Joueurs.SelectedIndex)
        If j.nbPartiesJouées = 0 Then
            MsgBox(j.nom & " n'a pas encore joué de partie.", MsgBoxStyle.Information +
				MsgBoxStyle.OkOnly, "Statistiques de " & j.nom)
        Else
            Dim stats As String = ""
            stats &= j.nom & " a joué " & j.nbPartiesJouées & " partie" & 
			If(j.nbPartiesJouées > 1, "s", "") & " pour un temps de jeu de " &
			toMinSec(j.tempsDeJeu) & vbCr & "Meilleur temps : " &
			toMinSec(j.meilleurTemps)
            MsgBox(stats, MsgBoxStyle.Information + MsgBoxStyle.OkOnly,
			"Statistiques de " & j.nom)
        End If
    End Sub
End Class