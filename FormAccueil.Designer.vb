﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormAccueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lbl_Title = New System.Windows.Forms.Label()
        Me.lbl_Nom = New System.Windows.Forms.Label()
        Me.btn_Jouer = New System.Windows.Forms.Button()
        Me.btn_Options = New System.Windows.Forms.Button()
        Me.btn_Quitter = New System.Windows.Forms.Button()
        Me.ComboB_Nom = New System.Windows.Forms.ComboBox()
        Me.btn_Scores = New System.Windows.Forms.Button()
        Me.PB_Image = New System.Windows.Forms.PictureBox()
        CType(Me.PB_Image, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_Title
        '
        Me.lbl_Title.AutoSize = True
        Me.lbl_Title.Font = New System.Drawing.Font("Garamond", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Title.Location = New System.Drawing.Point(87, 21)
        Me.lbl_Title.Name = "lbl_Title"
        Me.lbl_Title.Size = New System.Drawing.Size(180, 54)
        Me.lbl_Title.TabIndex = 1
        Me.lbl_Title.Text = "Memory"
        '
        'lbl_Nom
        '
        Me.lbl_Nom.AutoSize = True
        Me.lbl_Nom.Location = New System.Drawing.Point(73, 195)
        Me.lbl_Nom.Name = "lbl_Nom"
        Me.lbl_Nom.Size = New System.Drawing.Size(61, 13)
        Me.lbl_Nom.TabIndex = 3
        Me.lbl_Nom.Text = "Votre nom :"
        '
        'btn_Jouer
        '
        Me.btn_Jouer.Location = New System.Drawing.Point(95, 283)
        Me.btn_Jouer.Name = "btn_Jouer"
        Me.btn_Jouer.Size = New System.Drawing.Size(75, 23)
        Me.btn_Jouer.TabIndex = 2
        Me.btn_Jouer.Text = "Jouer"
        Me.btn_Jouer.UseVisualStyleBackColor = True
        '
        'btn_Options
        '
        Me.btn_Options.Location = New System.Drawing.Point(176, 283)
        Me.btn_Options.Name = "btn_Options"
        Me.btn_Options.Size = New System.Drawing.Size(75, 23)
        Me.btn_Options.TabIndex = 4
        Me.btn_Options.Text = "Options"
        Me.btn_Options.UseVisualStyleBackColor = True
        '
        'btn_Quitter
        '
        Me.btn_Quitter.Location = New System.Drawing.Point(176, 312)
        Me.btn_Quitter.Name = "btn_Quitter"
        Me.btn_Quitter.Size = New System.Drawing.Size(75, 23)
        Me.btn_Quitter.TabIndex = 5
        Me.btn_Quitter.Text = "Quitter"
        Me.btn_Quitter.UseVisualStyleBackColor = True
        '
        'ComboB_Nom
        '
        Me.ComboB_Nom.FormattingEnabled = True
        Me.ComboB_Nom.Location = New System.Drawing.Point(140, 192)
        Me.ComboB_Nom.MaxDropDownItems = 4
        Me.ComboB_Nom.Name = "ComboB_Nom"
        Me.ComboB_Nom.Size = New System.Drawing.Size(142, 21)
        Me.ComboB_Nom.Sorted = True
        Me.ComboB_Nom.TabIndex = 1
        '
        'btn_Scores
        '
        Me.btn_Scores.Location = New System.Drawing.Point(95, 312)
        Me.btn_Scores.Name = "btn_Scores"
        Me.btn_Scores.Size = New System.Drawing.Size(75, 23)
        Me.btn_Scores.TabIndex = 3
        Me.btn_Scores.Text = "Scores"
        Me.btn_Scores.UseVisualStyleBackColor = True
        '
        'PB_Image
        '
        Me.PB_Image.Location = New System.Drawing.Point(87, 77)
        Me.PB_Image.Name = "PB_Image"
        Me.PB_Image.Size = New System.Drawing.Size(180, 88)
        Me.PB_Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PB_Image.TabIndex = 6
        Me.PB_Image.TabStop = False
        '
        'FormAccueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(354, 350)
        Me.Controls.Add(Me.PB_Image)
        Me.Controls.Add(Me.btn_Scores)
        Me.Controls.Add(Me.ComboB_Nom)
        Me.Controls.Add(Me.btn_Quitter)
        Me.Controls.Add(Me.btn_Options)
        Me.Controls.Add(Me.btn_Jouer)
        Me.Controls.Add(Me.lbl_Nom)
        Me.Controls.Add(Me.lbl_Title)
        Me.Name = "FormAccueil"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Memory"
        CType(Me.PB_Image, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_Title As Label
    Friend WithEvents lbl_Nom As Label
    Friend WithEvents btn_Jouer As Button
    Friend WithEvents btn_Options As Button
    Friend WithEvents btn_Quitter As Button
    Friend WithEvents ComboB_Nom As ComboBox
    Friend WithEvents btn_Scores As Button
    Friend WithEvents PB_Image As PictureBox
End Class
